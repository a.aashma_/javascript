// Regex =>  Regular Expressions => a sequence of characters that forms a search pattern
//       /pattern/modifiers
// Aashma => /aashma/i 
// i => search to be case insentive

let text = "Hi AaShma!";
let result = text.replace(/aashma/i, "Ram");
console.log(result);


let pattern = /x/;
let res = pattern.test("Hey how are u!");
console.log(res);