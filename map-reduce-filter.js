"string".toLowerCase();

// Higher order function of array

// Map  array as argument returns an array

let data = [1, 2, 3, 4];

// The map() method is used for creating a new 
// array from an existing one, applying a function
// to each one of the elements of the first array.
function addTen(item) {
  return item * 10 + 10;
}

let updatedData = data.map(addTen);

console.log(updatedData);


// Reduce
// The reduce() method reduces an array of values down to just one value.

let reducedSum = data.reduce(function (result, item) {
  return result + item
}, 0);
// 0 => 0 + 1 => 1 + 2 => 3 + 3

console.log(reducedSum);

let fruits = ["apple", "bana", "mango"];
console.log(fruits.reduce(function (result, item) {
  return result + " " + item;
}, ""));



// FIlter
// The filter() method takes each element in
// an array and it applies a conditional statement against it. 
let numbs = [1, 2, 3, 4, 5, 3];
let filterdArr = numbs.filter(function (item) {
  return item === 3; // true or false
});
console.log(filterdArr);