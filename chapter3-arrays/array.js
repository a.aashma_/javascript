//Arrays  => container for data types in a particular order (0 -1) left to right
// "AASHMA"  => "A" "A" "S" "H" "M" "A" // char

// arrays index start from 0
let fruits = ["apple", "mango", "banana"]; // similar data type
let oddNumbers = [13, 5, 7, 19];
let trues = [true, true, true]; // instanciate

let apple = fruits[0]; // access
console.log(apple);


// Array slicing
// fruits.splice(1);
console.log(fruits);
console.log(fruits.length);
console.log(fruits[fruits.length - 1]);

// sort
console.log(oddNumbers.sort());
console.log(oddNumbers.reverse());

// adding and removing
fruits.push("momo", "pizza");
console.log(fruits);
fruits.pop()
console.log(fruits);

console.log(fruits.includes("mango"));

const str = "My name is aashma";
console.log(str.split(" "));


// iteration
for (let i = 0; i < fruits.length; i++) {
  console.log(fruits[i]);
}

// Ex
let a = ["ant", "bat", "rat"]
let total = "";
for (let i = 0; i < a.length; i++) {
  total = `${total}-${a[i]}`;
}
console.log(total);