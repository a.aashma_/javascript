# JavaScript

It includes the basic practices of JavaScript.
- Introduction to Javascript [DONE]
- String [DONE]
- Math and Number [DONE]
- Dates [DONE]
- Regular Expression
- Plain Object [DONE]
- Array [DONE]
- Functions 
- Map
- Filter
- Reduce
- Object and Prototype
- Event and DOM Manipulation