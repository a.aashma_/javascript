let today = new Date();
console.log(today);

console.log(Date.now());
// 4 ways
console.log(new Date());
console.log(new Date("2021", "10", "21"));
console.log(new Date("October 13, 2014 11:13:00"));


console.log(new Date(1632739616567));
console.log(new Date(000000000000));

// Formats
console.log(new Date("2015/03/25")); // YYYY/MM/DD
console.log(new Date("25-03-2015")); // DD-MM-YYYY
console.log(new Date("Mar 25 2015")); // MMM DD YYYY


// Getters
console.log("Full Year", today.getFullYear());
console.log("Month", today.getMonth());
console.log(today.getHours());


// Setters
console.log(today.setFullYear("2018"));
console.log(new Date(1538045534921));