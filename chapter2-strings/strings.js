let firstName = "Aashma"; // string
let lastName = `Dahal`;
let work = 'Bajra';

console.log(firstName + " " + lastName); // string concatination
console.log(`My name is ${firstName} ${lastName}!!!`); // string intepolation

// Exercise 2.2
let city = "Kathmandu";
var state = "Bagmati";

// string concatination
console.log(`${city}, ${state}`);
console.log(`${city},\t${state}`);

// string intepolation
console.log(city + ", " + state);
console.log(city + ",\t" + state);