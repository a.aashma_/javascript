let password = "aashma";

if (password.length < 6) {
  console.log("Password is too short");
} else {
  console.log("Password is OK");
}

// Combining and inverting booleans

// && (and => both have to be true) || (or - one must be true) ! (bang or not - opossite)


let a = 10;
let b = 20;

if (a > 100 || b > 18) { // both true
  console.log("YES!");
} else {
  console.log("NO!");
}

let x = "foo";
let y = "";

// AND
if (x.length === 0 && y.length === 0) {
  console.log("Both strings are empty!");
} else {
  console.log("At least one of the strings is nonempty.");
}

//  OR
if (x.length === 0 || y.length === 0) {
  console.log("At least one of the strings is empty!");
} else {
  console.log("Neither of the strings is empty.");
}

// NOT
if (!(x.length === 0)) { // if(x.length !== 0){}
  console.log("X is not empty");
} else {
  console.log("X is empty");
}