// Data types

// variables =>  a variable is a value that can change!
//  123 (interger) 123.123 -> decimal(float or double)
// "Aashma" => string, 'A' => character (char)
// true/false => boolean
let aNumber = 12;
let aString = "Aashma";
let aDecimal = 12.02;
let aChar = 'a';
let booleanValue = true;


let aashmaLength = aString.length;
console.log(aashmaLength);
console.log(typeof aNumber, typeof aString, typeof aDecimal, typeof aChar, typeof booleanValue);
let testValue = aashmaLength < 10;
console.log(testValue);

console.log(1 == 1); // > < >= <= == != 

console.log("1" === 1);