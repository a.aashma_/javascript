console.log(1 + 1);
console.log(2 - 1);
console.log(10 / 5);
console.log(10 * 2);
console.log(10 ** 2);

console.log(10 % 4); // modulus (reminder)

let a = 2;
console.log(a++);
console.log(a);

// Maths
console.log(Math.PI, Math.SQRT2, Math.LN10);


console.log(Math.round(10.00000002));
console.log(Math.pow(8, 2));
console.log(Math.sqrt(64));

console.log(Math.max(0, 150, 30, 20, -8, -200));

console.log(Math.random());