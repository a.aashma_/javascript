// Prototype => All JavaScript objects inherit properties and methods from a prototype.

function Vehicle(name, modal) {
  this.name = name;
  this.modal = modal;
}

let car = new Vehicle("CAR", "blue");
let bus = new Vehicle("BUS", "black");

// properties
Vehicle.prototype.wheels = 4;
// methodds
Vehicle.prototype.start = function () {
  return `Starting ${this.name}`;
};

console.log(bus.start());
console.log(car.wheels);