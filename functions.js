// Function is a block of code designed to perform a particular task
// executed when "something" invokes it (calls it).

// local variables and global variables
// gloabl variable
let a = 2;

// reusable code (piece of code)
function greeting(name) {
  console.log("Global variable =>", a);
  console.log(`Hi ${name}, how are you?`);
}

function anotherOne() {
  console.log("Global variable =>", a);
}

// arguments "Aaashma"
// paramters name
greeting("Aaashma");
greeting("Ram");
anotherOne();


// Return keyword


function add(num1, num2) {

  console.log("Adding " + num1 + " " + num2);
  return num1 + num2;
}


function toCelsius(fahrenheit) {
  return (5 / 9) * (fahrenheit - 32);
}

let sum = add;
console.log(sum(1, 100));

console.log(toCelsius(129));
console.log(toCelsius(229));