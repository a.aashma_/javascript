// object => properties and methods 

// example => human -> color, size, gender , methods => eats(), walk()

let aashma = { // JSON
  color: "brown",
  size: "100kg",
  gender: "female"
};
// instantiate
const person = {
  firstName: "John",
  lastName: "Doe",
  age: 50,
  eyeColor: "blue",
  eat: function () {
    console.log("I am eating");
  },
  walk: function (km) {
    console.log(`I am walking ${km}km`);
  }
};

console.log(person);

// accessing
console.log(person.age);
console.log(person["firstName"]);

// set

person.age = 1000;

console.log(person);
person.eat();

person.walk(12);